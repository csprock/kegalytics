import serial
from utils import parse_message
import time

class ArduinoInterface:

    def __init__(self, port, baudrate, timeout, max_retry, retry_timeout):
        
        self.SERIAL_READER = serial.Serial(port = port, baudrate = baudrate, timeout = timeout)
        self.max_retry = max_retry
        self.retry_timeout = retry_timeout
        self.last_good_count = int()
        self.retries = 0

    def get_results(self, i = None, cp = False):

        results = self.SERIAL_READER.readline()

        if i != None:
            payload = parse_message(results, i = i, cp = cp)
        else:
            payload = parse_message(results, cp = cp)

        self.last_good_count = payload['count']

        return payload


    def get_message(self, i = None, cp = False):

        try:
            payload = self.get_results(i = i, cp = cp)
            self.retries = 0
            return payload
        except serial.serialutil.SerialException:
            self.retries += 1
            if self.retries < self.max_retry:
                self.SERIAL_READER.close()
                print("Exception raised, retry number: %s" % self.retries)
                time.sleep(self.retry_timeout)
                try:
                    self.SERIAL_READER.open()
                    self.SERIAL_READER.reset_input_buffer()
                    self.SERIAL_READER.write(str.encode(str(self.last_good_count)))
                except:
                    pass
                finally:
                    payload = self.get_message(i = i, cp = cp)
                    return payload

            else:
                raise ValueError("Number of retries exceeded.")
