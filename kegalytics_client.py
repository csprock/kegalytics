import paho.mqtt.client as mqtt
from arduino_interface import ArduinoInterface
from utils import on_connect, get_certificate_files
import json

class KegalyticsClient:

    def __init__(self, client_id, host, mqtt_port, serial_port, serial_baudrate = 9600, serial_timeout = None, cert_path = None, max_retry = 10, retry_timeout = 5, qos = 0):

        self.MQTT_CLIENT = mqtt.Client(client_id = client_id, clean_session = True)
        self.MQTT_CLIENT.on_connect = on_connect

        self.qos = qos
        self.host = host
        self.port = mqtt_port

        if cert_path != None:
            certs = get_certificate_files(cert_path)
            client.tls_set(ca_certs = certs['ca_cert'], keyfile = certs['keyfile'], certfile = certs['certfile'])

        self.ARDUINO = ArduinoInterface(port = serial_port, baudrate = serial_baudrate, timeout = serial_timeout, max_retry = max_retry, retry_timeout = retry_timeout)

        self.JSON_ENCODER = json.JSONEncoder()


    def start(self, topic, fake = False, add_index = False, verbose = False):

        self.MQTT_CLIENT.connect(host = self.host, port = self.port)
        self.MQTT_CLIENT.loop_start()
        i = 0
        last_count = int()

        while True:

            if add_index:
                payload = self.ARDUINO.get_message(i = i, cp = fake)
            else:
                payload = self.ARDUINO.get_message(i = None, cp = fake)

            if fake:
                i += 4
                for pld in payload:
                    if verbose: print(pld)
                    self.MQTT_CLIENT.publish(topic = topic, payload = self.JSON_ENCODER.encode(pld), qos = self.qos)
            else:
                i += 1
                if verbose: print(payload)
                self.MQTT_CLIENT.publish(topic = topic, payload = self.JSON_ENCODER.encode(payload), qos = self.qos)
