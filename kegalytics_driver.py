import json
import argparse
import time
import paho.mqtt.client as mqtt
from arduino_interface import ArduinoInterface
from utils import get_certificate_files, on_connect
import copy
parser = argparse.ArgumentParser()

parser.add_argument('--verbose', action = 'store_true')

# Arguments for configuring MQTT client
parser.add_argument('--topic')
parser.add_argument('--port', type = int, default = 8883)
parser.add_argument('--host', default = 'a2wpdjuo7ku76e.iot.us-west-2.amazonaws.com')
parser.add_argument('--client_id')
parser.add_argument('--qos', type = int, choices = [0,1,2], default = 0)
parser.add_argument('--cert_path')
parser.add_argument('--add_index', default = False, action = 'store_true')
parser.add_argument('--max_retries', default = 10)
parser.add_argument('--retry_wait', default = 5)

# Arguments for configuring serial port reader
parser.add_argument('--serial_port')
parser.add_argument('--baudrate', type = int, default = 9600)
parser.add_argument('--serial_timeout', type = int)
parser.add_argument('--fake', default = False, action = 'store_true')

if __name__ == '__main__':

    args = parser.parse_args()



    ####### MQTT Client #######


    client = mqtt.Client(client_id = args.client_id, clean_session = True)
    client.on_connect = on_connect

    # If path to directory containing certificates provided, set TLS/SSL on client using certificates
    # found the provided directory. See docstring of get_certificate_files().
    if args.cert_path != None:
        certs = get_certificate_files(args.cert_path)
        client.tls_set(ca_certs = certs['ca_cert'], keyfile = certs['keyfile'], certfile = certs['certfile'])

    ####### Serial Port Reader #######
    arduino_interface = ArduinoInterface(port = args.serial_port, baudrate = args.baudrate, timeout = args.serial_timeout, max_retry = args.max_retries, retry_timeout = args.retry_wait)

    ####### json encoder #####
    json_encoder = json.JSONEncoder()

    # Connect to host and start transmitting data
    client.connect(host = args.host, port = args.port)
    client.loop_start()
    i = 0
    last_count = int()

    while True:

        if args.add_index:
            payload = arduino_interface.get_message(i = i, cp = args.fake)
        else:
            payload = arduino_interface.get_message(i = None, cp = args.fake)


        if args.fake:
            i += 4
            for pld in payload:
                if args.verbose: print(pld)
                client.publish(topic = args.topic, payload = json_encoder.encode(pld), qos = args.qos)
        else:
            i += 1
            if args.verbose: print(payload)
            client.publish(topic = args.topic, payload = json_encoder.encode(payload), qos = args.qos)
