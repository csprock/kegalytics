import json
import argparse
import time
from kegalytics_client import KegalyticsClient

parser = argparse.ArgumentParser()

parser.add_argument('--verbose', action = 'store_true')

# Arguments for configuring MQTT client
parser.add_argument('--topic')
parser.add_argument('--port', type = int, default = 8883)
parser.add_argument('--host', default = 'a2wpdjuo7ku76e.iot.us-west-2.amazonaws.com')
parser.add_argument('--client_id')
parser.add_argument('--qos', type = int, choices = [0,1,2], default = 0)
parser.add_argument('--cert_path')
parser.add_argument('--add_index', default = False, action = 'store_true')
parser.add_argument('--max_retries', default = 10)
parser.add_argument('--retry_wait', default = 5)

# Arguments for configuring serial port reader
parser.add_argument('--serial_port')
parser.add_argument('--baudrate', type = int, default = 9600)
parser.add_argument('--serial_timeout', type = int)
parser.add_argument('--fake', default = False, action = 'store_true')


if __name__ == '__main__':

    args = parser.parse_args()

    client = KegalyticsClient(client_id = args.client_id,
                                host = args.host,
                                mqtt_port = args.port,
                                serial_baudrate = args.baudrate,
                                serial_timeout = args.serial_timeout,
                                serial_port = args.serial_port,
                                qos = args.qos,
                                cert_path = args.cert_path,
                                max_retry = args.max_retries,
                                retry_timeout = args.retry_wait)
                                
    client.start(topic = args.topic, fake = args.fake, add_index = args.add_index, verbose = args.verbose)
