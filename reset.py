import serial
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--serial_port')
parser.add_argument('--baudrate', type = int, default = 9600)
parser.add_argument('--timeout', type = int)
parser.add_argument('--message', default = 1)
parser.add_argument('--verbose', action = 'store_true')

def reset_arduino(serial_port, baudrate):
    serial_writer = serial.Serial(port = serial_port, baudrate = baudrate)
    serial_writer.reset_input_buffer()
    serial_writer.write(b'0')

if __name__ == '__main__':
    args = parser.parse_args()
    serial_writer = serial.Serial(port = args.serial_port, baudrate = args.baudrate, write_timeout = args.timeout)
    serial_writer.reset_input_buffer()
    serial_writer.write(b'0')

