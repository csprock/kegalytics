from os import listdir
from os.path import isfile, join
import re
import datetime
import time
import os
import copy

def get_certificate_files(path):
    ''' given path to containing certificate files, returns full path names to each'''
    def get_ca_cert(files):
        for f in files:
            if re.match('root\.pem\.crt', f) != None:
                return join(path, f)

        raise ValueError("Error! No file with extension .pem found in %s" % path)

    def get_certfile(files):
        for f in files:
            if re.match('.+certificate\.pem\.crt', f) != None:
                return join(path, f)

        raise ValueError("Error! No file with extension .pem.crt found in %s" % path)

    def get_keyfile(files):
        for f in files:
            if re.match('.+private\.pem\.key', f) != None:
                return join(path, f)
        raise Valuerror("Error! No file with extension .pem.key found in %s" % path)

    files = [f for f in listdir(path) if isfile(os.path.join(path, f))]

    return {'ca_cert':get_ca_cert(files),
            'keyfile':get_keyfile(files),
            'certfile':get_certfile(files)}


def timestamp(which = 'unix'):
    pad = lambda x: str(x).zfill(2) if len(str(x)) < 2 else str(x)
    if which == 'unix':
        return int(round(time.time()))
    elif which == 'string':
        dt = datetime.datetime.now()
        ts = "{}-{}-{} {}:{}:{}".format(dt.year, pad(dt.month), dt.year, pad(dt.hour), pad(dt.minute), pad(dt.second))
        return ts
    elif which == 'timestamp':
        return datetime.datetime.now()

def parse_message(mssg, i = None, cp = False):
    '''
    Parses message returned by serial reader, adds timestamp and encodes with json.
    '''
    relu = lambda x: x if x >= 0 else 0.0
    mssg = mssg.decode('utf-8').replace('\r\n','').split(' ')
    mssg = {'timestamp':timestamp(), 'tap':mssg[1], 'flow':relu(float(mssg[3])), 'count':int(mssg[5])}
    #mssg = {'message':mssg}
    if i != None: mssg['id'] = i

    if cp:
        mssgs = [mssg]
        for j in range(1,4):
            temp = copy.copy(mssg)
            temp['tap'] = str(j+1)
            if i != None:
                temp['id'] += j
            mssgs.append(temp)
        return mssgs
    else:
        return mssg


def on_connect(client, userdata, flags, rc):
    print("inside on_connect")
    if rc != 0:
        raise ValueError("Failed to connect. Status: %s" % str(rc))
    else:
        print("Connection OK")
